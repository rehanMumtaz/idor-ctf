import userResolver from './user.resolver.js';
import exploitResolver from './exploit.resolver.js';

export default [userResolver, exploitResolver];
