import exploitTypeDefs from './typeDefs/exploit.typeDefs.js';
import userTypeDefs from './typeDefs/user.typeDefs.js';
import exploitSchema from './exploit.schema.js';
import userSchema from './user.schema.js';
//
export default [exploitTypeDefs, exploitSchema, userTypeDefs, userSchema];
