import gql from 'graphql-tag';

const userSchema = gql`
  input SignupInput {
    email: String!
    password: String!
    fname: String!
    lname: String
  }

  input LoginInput {
    email: String!
    password: String!
  }

  type Query {
    GetUsers(total: Int): [User]
    GetUserById(id: ID!): User!
  }

  type JwtToken {
    token: String!
  }

  type UserWithToken {
    _id: String
    email: String
    fname: String
    lname: String
    following: [String]
    createdAt: DateTime
    updatedAt: DateTime
    userJwtToken: JwtToken
  }

  type Mutation {
    Login(input: LoginInput): UserWithToken
    Signup(input: SignupInput): UserWithToken
  }
`;

export default userSchema;
